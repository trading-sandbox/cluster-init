# cluster-init

## Description
Collection of K8s manifests for single-node configuration (minikube) for linux based systems

## Requirements
1. Docker
2. kubectl
3. helm
4. minikube
